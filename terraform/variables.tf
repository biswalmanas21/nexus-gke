variable "name" {
  type        = string
  description = "Name of the cluster"
}
variable "project_id" {
  type        = string
  description = "Name of the cluster"
}

variable "region" {
  type        = string
  description = "Name of the region where cluster is available"
}


variable "network" {
  type        = string
  description = "Network name where it is installed"
}

variable "zone" {
  type        = string
  description = "What zone the node belongs to"
}

variable "initial_node_count" {
  type        = number
  description = "Number of nodes on initialization"
}


variable "nodepoolname" {
  type        = string
  description = "What is the name of the nodepool"
}
variable "credentials" {
  type        = string
  description = "credential file location"
}
variable "machine_type" {
  type        = string
  description = "What type of machine to be used for nodepool"
}

variable "nexus_bucket" {
  type        = string
  description = "description"
}
