credentials        = "./adc.json"
project_id         = "aliz-hw-manasbiswal"
region             = "europe-west4"
zone               = "europe-west4-a"
name               = "nexus-cluster"
nodepoolname       = "nexus-nodepool"
machine_type       = "n1-standard-1"
initial_node_count = 1

network      = "default"
nexus_bucket = "aliz-hw-manasbiswal-nexus-eu"
