resource "google_container_cluster" "primary" {
  provider                 = "google-beta"
  name                     = var.name
  location                 = var.region
  network                  = var.network
  remove_default_node_pool = true
  initial_node_count       = var.initial_node_count
  master_auth {
    username = ""
    password = ""

    client_certificate_config {
      issue_client_certificate = false
    }
  }
  addons_config {
    http_load_balancing {
      disabled = false
    }
    istio_config {
      disabled = false
    }
  }
}

resource "google_container_node_pool" "primary_preemptible_nodes" {
  name       = var.nodepoolname
  location   = var.region
  cluster    = google_container_cluster.primary.name
  node_count = var.initial_node_count

  node_config {
    preemptible  = true
    machine_type = var.machine_type

    metadata = {
      disable-legacy-endpoints = "true"
    }

    oauth_scopes = [
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
      "https://www.googleapis.com/auth/devstorage.read_only",
      "https://www.googleapis.com/auth/servicecontrol"
    ]
  }
}
resource "google_storage_bucket" "nexus_bucket" {
  name     = var.nexus_bucket
  location = var.region
}
