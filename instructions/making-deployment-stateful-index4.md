## Requirement:

> Handle Pod failure

> Maintain initial config for authentication

> Maintain state and database 

*To handle all of the above, current setup is using persistent volume. As there is only one node, dynamically claimed volume is used*

**current AccessMode: ReadWriteOnce**

*If we add more nodes in the future, we will have to use AccessMode as : ReadOnlyMany for compute based persistent disks or ReadWriteMany for GFS*

Changed Files:

    - deployment.yaml
    - pvc.yaml
    - values.yaml

- All Data is saved in location :   /nexus-data 

- Mount point is defined as /nexus-data with the same gid as the container to maintain permission level in the persistent disk.


Q:

- How do I see the persistent volume and claim

```
kubectl get pv    :- for persistent volume
kubectl get pvc -n ${namespace} :- for persistent volume claim
```

Q:
- Why have we not used statefulSet ?

> statefulSet makes more sense for stateful applications and databases with high availability. Since we have one one node, we can use deployment with persistent disk of type ReadWriteOnce. For production grade application, we will need multi-node cluster with persistent disks, statefulSet replicas and data replication across multiple nodes with a headless service.