## Requirement :

> Create GKE cluster

> Create Nodepool

> Create Cloud Storage Bucket

**Create GKE cluster**

*The Account was not permitted to perform any IAM , Service Account operations. All activities performed are created and maintained by the default GOOGLE_APPLICATION_CREDENTIALS.*

*The GKE module requires separate service account to be created at runtime. To avoid that, gke module by hashicorp or google were not used. All required resources are created via base resouce definitions in terraform*

*Stages*
 - create GKE by "google_container_cluster" . provider used : "google-beta"  , as istio add-on is only availeble on google-beta at this moment.

 - create Nodepool in a specific region and specified zone.(not recommended.)Details of the nodepool can be seen in file main.tf

 - create Google cloud storage bucket for terraform backend. reference file: terraform.tf

 - create Google cloud storage bucket for private repository of nexus3 sonatype. reference: main.tf

 variables data is present in variables.tf.
 Required values of variables.tf are replaced by **variables.auto.tfvars** at the runtime.

> How to Validate the terraform resources ?

- Login to the Kubernetes cluster created in Google cloud console

- Follow instructions to configure the cluster context

- Run command :

```
kubectl get nodes
kubectl get pods --all-namespaces
```
- If you see all nodes in Ready state and all pods in RUNNING state, you are good to start rest of the exercise


NOTE: Sometime in rare occassions if insufficient resources are allocated to a cluster, pods restart or go in to preemptive mode. It is absolutely safe to delete those pods, if same pods are already spawned in the cluster. 

- At the very end, you will be able to see 2 namespaces : **kube-system , istio-system**


