## Requirement :

> Verify Istio-system pods 

> Creating a new namespace for your deployment if not already available

> Enabling istio injection in the namespace previously created

> Creating firewalls for Ingress ports (HTTP/HTTPS)


Please refer script istio-initialize.sh for reference
