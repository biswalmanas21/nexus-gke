## Requirement:

> Istio Ingress Gateway

> Istio VirtualService

> nexus Service

> nexus Deployment


commands:

- Verify:

```
helm install --debug --dry-run --namespace ${namespace} --generate-name ./${application_name}
```

- Deploy
```
helm install --namespace ${namespace} --generate-name ./${application_name}
```

*Readiness and Liveness is set at 80s delay. The application takes a lil bit of time to start. Around 88s you should be able to see the application running along side istio.*

- Verify pod status

```
kubectl get pods -n ${namespace} -o wide
```

**Opening the URL**

> Finding out external IP:

*As we are using istio mesh here, The external IP will be associated with the istio loadbalancer present in istio-system namespace*

```
kubectl get svc -n istio-system

NAME                     TYPE

istio-ingressgateway     LoadBalancer
4h

```

*The external IP of this ingress gateway of type Loadbalancer is the entry point*

*URL:  "http://${EXTERNAL_IP}:80/${context}"*

*In our case context is : "/"*
