#!/bin/bash -xve

read -p "nexus-blobstore version : " version

url="https://repo1.maven.org/maven2/org/sonatype/nexus/plugins/nexus-blobstore-google-cloud/${version}/nexus-blobstore-google-cloud-${version}.kar"

outputname="nexus-blobstore-google-cloud-${version}.kar"

echo -e "Downloading the plugin $outputname"

curl -O $url

echo -e "finding GOOGLE_APPLICATION_CREDENTIALS"

cp -rp $HOME/.config/gcloud/legacy_credentials/*/adc.json gke_creds.json
