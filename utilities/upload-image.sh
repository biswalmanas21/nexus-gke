#!/bin/bash -xve

imageid=$1

docker tag  $imageid gcr.io/aliz-hw-manasbiswal/sonatype-nexus3:$imageid

docker push gcr.io/aliz-hw-manasbiswal/sonatype-nexus3:$imageid
