#!/bin/bash 


echo -e "\n=========================================================>"
echo -e "listing all pods inside the istio-system name-space"
kubectl get pods -n istio-system
echo -e "=========================================================>"


echo -e "\n=========================================================>"
echo -e "enabling istion injection in namespace nexus"
namespace="nexus-sonatype"
echo -e "=========================================================>"
receive=$(kubectl get namespaces | grep $namespace)
echo "Name space :  $receive"
if [[ -z $receive ]]; then
    echo -e "NameSpace :  $namespace does not exist"
    sleep 5
    echo -e "Creating namespace : $namespace"
    kubectl create namespace $namespace
fi

kubectl label namespace $namespace istio-injection=enabled

echo -e "\n=========================================================>"
echo -e "Allowing ingress Traffic from Loadbalancer"
echo -e "=========================================================>"
export INGRESS_PORT=$(kubectl -n istio-system get service istio-ingressgateway -o jsonpath='{.spec.ports[?(@.name=="http2")].nodePort}')
export SECURE_INGRESS_PORT=$(kubectl -n istio-system get service istio-ingressgateway -o jsonpath='{.spec.ports[?(@.name=="https")].nodePort}')
echo -e "PORTS ALLOWED :   $INGRESS_PORT    &    $SECURE_INGRESS_PORT "


echo -e "\n=========================================================>"
echo -e "Securing application:  Creating Firewalls"
echo -e "=========================================================>"
gcloud compute firewall-rules create allow-gateway-http --allow tcp:$INGRESS_PORT
gcloud compute firewall-rules create allow-gateway-https --allow tcp:$SECURE_INGRESS_PORT




